Y_MAKEDIR=/Users/frigaut/yorick-2.1/Darwin-Power_Macintosh
Y_EXE=/Users/frigaut/yorick-2.1/Darwin-Power_Macintosh/bin/yorick
Y_EXE_PKGS=
Y_EXE_HOME=/Users/frigaut/yorick-2.1/Darwin-Power_Macintosh
Y_EXE_SITE=/Users/frigaut/yorick-2.1

# ----------------------------------------------------- optimization flags

COPT=$(COPT_DEFAULT)
TGT=$(DEFAULT_TGT)

# ------------------------------------------------ macros for this package

PKG_NAME=curses
PKG_I=curses.i

OBJS=curses.o

# change to give the executable a name other than yorick
PKG_EXENAME=yorick

# PKG_DEPLIBS=-Lsomedir -lsomelib   for dependencies of this package
PKG_DEPLIBS=-lcurses
# set compiler or loader (rare) flags specific to this package
PKG_CFLAGS=
PKG_LDFLAGS=

# list of additional package names you want in PKG_EXENAME
# (typically Y_EXE_PKGS should be first here)
EXTRA_PKGS=$(Y_EXE_PKGS)

# list of additional files for clean
PKG_CLEAN=

# autoload file for this package, if any
PKG_I_START=

# -------------------------------- standard targets and rules (in Makepkg)

# set macros Makepkg uses in target and dependency names
# DLL_TARGETS, LIB_TARGETS, EXE_TARGETS
# are any additional targets (defined below) prerequisite to
# the plugin library, archive library, and executable, respectively
PKG_I_DEPS=$(PKG_I)

include $(Y_MAKEDIR)/Make.cfg
include $(Y_MAKEDIR)/Makepkg
include $(Y_MAKEDIR)/Make$(TGT)

# override macros Makepkg sets for rules and other macros
# Y_HOME and Y_SITE in Make.cfg may not be correct (e.g.- relocatable)
Y_HOME=$(Y_EXE_HOME)
Y_SITE=$(Y_EXE_SITE)

# reduce chance of yorick-1.5 corrupting this Makefile
MAKE_TEMPLATE = protect-against-1.5

# ------------------------------------- targets and rules for this package

# simple example:
#myfunc.o: myapi.h
# more complex example (also consider using PKG_CFLAGS above):
#myfunc.o: myapi.h myfunc.c
#	$(CC) $(CPPFLAGS) $(CFLAGS) -DMY_SWITCH -o $@ -c myfunc.c

# -------------------------------------------------------- end of Makefile

# for the binary package production:
PKG_DEPLIBS_STATIC=-lm -lcurses 
PKG_OS = macosx
# or linux or windows
PKG_VERSION = 0.1

binaries:
	$(MAKE)
	$(LD_DLL) -o $@ $(OBJS) ywrap.o $(PKG_DEPLIBS_STATIC) $(DLL_DEF)
	mv binaries $(PKG_NAME).so

package:
	$(MAKE) binaries
	mkdir -p binaries/$(PKG_NAME)/dist/y_home/lib
	mkdir -p binaries/$(PKG_NAME)/dist/y_home/i-start
	mkdir -p binaries/$(PKG_NAME)/dist/y_site/i0
	cp -p *.i binaries/$(PKG_NAME)/dist/y_site/i0/
	cp -p check.i binaries/$(PKG_NAME)/.
	rm binaries/$(PKG_NAME)/dist/y_site/i0/check.i
	if test -n "$(PKG_I_START)"; then rm binaries/$(PKG_NAME)/dist/y_site/i0/$(PKG_I_START); fi
	cp -p $(PKG_NAME).so binaries/$(PKG_NAME)/dist/y_home/lib/
	if test -n "$(PKG_I_START)"; then cp -p $(PKG_I_START) binaries/$(PKG_NAME)/dist/y_home/i-start/; fi
	cat $(PKG_NAME).info | sed -e 's/OS:/OS: $(PKG_OS)/' > tmp.info
	mv tmp.info binaries/$(PKG_NAME)/$(PKG_NAME).info
	cd binaries; tar zcvf $(PKG_NAME)-$(PKG_VERSION)-$(PKG_OS).tgz $(PKG_NAME)

distrib:
	if test -f "binaries/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_OS).tgz" ; then ncftpput -f /Users/frigaut/.ncftp/maumae www/yorick/packages/$(PKG_OS)/tarballs/ binaries/$(PKG_NAME)-$(PKG_VERSION)-$(PKG_OS).tgz; fi
	if test -f "binaries/$(PKG_NAME)/$(PKG_NAME).info" ; then ncftpput -f /Users/frigaut/.ncftp/maumae www/yorick/packages/$(PKG_OS)/info/ binaries/$(PKG_NAME)/$(PKG_NAME).info; fi
	make clean
	cd ..; tar --exclude binaries --exclude .svn -zcvf $(PKG_NAME)-$(PKG_VERSION)-src.tgz $(PKG_NAME); ncftpput -f /Users/frigaut/.ncftp/maumae www/yorick/packages/src/ $(PKG_NAME)-$(PKG_VERSION)-src.tgz 


# -------------------------------------------------------- end of Makefile
