plug_dir,".";
require,"curses.i";
require,"util_fr.i";
require,"random.i";

func testloop(void)
{
  kbdinit; cerase;
  cprintw,"%s"," Press 'q' to end, space to pause";
  tic(3);
  niter = 3000;
  fromzen = 0.f;
  tozen = 50.f;
  zen = span(fromzen,tozen,niter);
  nc = cgetcols();
  
  for (i=1;i<=niter;i++) {
  
    r = string(&char(kbd(0)));
    if (r=="q") break;
    if (r==" ") { // pause
      cbeep;
      cmvprintw,0,0,"%s"," Press any key to resume"; cclrtoeol; crefresh;
      r=string(&char(kbd(1)));
      cmvprintw,0,0,"%s"," Press 'q' to end, space to pause"; crefresh;
    }
    line = 1;
    cmvprintw,line,0,"%s",swrite(format="Elevation: %.2f -> %.2f -> %.2f",fromzen,
                                zen(i),tozen);
    cclrtoeol;
    line++;

    for (n=1;n<=2;n++) { 
      line++;
      cmvprintw,line,0,"%s",swrite(format="%-17s  tip=%+7.2f'', tilt=%+7.2f''",
                                  "Mechanism",random_n(1)(1),random_n(1)(1));
      cclrtoeol;
    }
    progressbar,i,niter,15,counter=3,line=1;
    ccurs_set,0;
    ccurs_move,0,0;
    crefresh;
  }
  kbdend;
}

func enterstr(void)
{
  kbdinit; cerase; cecho;
  s= array(" ",1024)(sum);
  cprintw,"%s","Enter some string: ";
  cgetstr,s;
  kbdend;
  print,"You entered "+s;
  pause,1000;
}

func testatom(void)
{
  cinitscr;
  ccbreak;
  cnoecho;
  cnonl;
  cintrflush,1;
  ckeypad,1;
  cnodelay,0;
  nl = cgetlines();
  cmvprintw,nl-1,0,"%s","Hit a key to start";
  //  ccurs_move,0,0;
  r = cgetch();
  cendwin;
  return string(&char(r));
}

func testscroll(void)
  // the scroll does not produce the desired effect.
  // i must have forgotten something or not understood
  // the nature of "scrollok"
{
  cinitscr; cerase; cclear; ccbreak;
  cscrollok,1; cidlok,1;
  text = rdfile("check-plug.i");
  for (i=1;i<=numberof(text);i++) {
    cprintw,"%s\n",text(i);
  }
  cprintw,"%s","\nhit a key when ready";
  cnodelay,0;
  r = cgetch();
  cendwin;
}

testatom;
//testscroll;

enterstr;
//countdown,3;
testloop;
write,"Tests successful"
