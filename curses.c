#include <curses.h>
#include <stdlib.h>

void kbdinit() { 
  initscr(); 
  cbreak(); 
  noecho();
  nonl();
  intrflush(stdscr, TRUE);
  keypad(stdscr, TRUE);
}

int kbd(int wait)
{
  int kb;
  if (!wait) nodelay(stdscr, TRUE); else nodelay(stdscr, FALSE);
  kb = getch();
  nodelay(stdscr, FALSE);
  return kb;
}

void kbdend() { endwin(); }

int getlines() {return LINES; }
int getcols() {return COLS; }

int ynodelay(int condition)
{
  if (condition) nodelay(stdscr, TRUE); else nodelay(stdscr, FALSE);
}
int ynotimeout(int condition)
{
  if (condition) notimeout(stdscr, TRUE); else notimeout(stdscr, FALSE);
}
int ykeypad(int condition)
{
  if (condition) keypad(stdscr, TRUE); else keypad(stdscr, FALSE);
}
int ymeta(int condition)
{
  if (condition) meta(stdscr, TRUE); else meta(stdscr, FALSE);
}
int yintrflush(int condition)
{
  if (condition) intrflush(stdscr, TRUE); else intrflush(stdscr, FALSE);
}
int yscrollok(int condition)
{
  if (condition) scrollok(stdscr, TRUE); else scrollok(stdscr, FALSE);
}
int yleaveok(int condition)
{
  if (condition) leaveok(stdscr, TRUE); else leaveok(stdscr, FALSE);
}
int yclearok(int condition)
{
  if (condition) clearok(stdscr, TRUE); else clearok(stdscr, FALSE);
}
int yidlok(int condition)
{
  if (condition) idlok(stdscr, TRUE); else idlok(stdscr, FALSE);
}
