/* curses Yorick plugin, version 0.1
 * Francois Rigaut, nov2004
 * last revision/addition: 2004Nov30
 *
 * Copyright (c) 2003, Francois RIGAUT (frigaut@maumae.net)
 *
 * This program is free software; you can redistribute it and/or  modify it
 * under the terms of the GNU General Public License  as  published  by the
 * Free Software Foundation; either version 2 of the License,  or  (at your
 * option) any later version.
 *
 * This program is distributed in the hope  that  it  will  be  useful, but
 * WITHOUT  ANY   WARRANTY;   without   even   the   implied   warranty  of
 * MERCHANTABILITY or  FITNESS  FOR  A  PARTICULAR  PURPOSE.   See  the GNU
 * General Public License for more details (to receive a  copy  of  the GNU
 * General Public License, write to the Free Software Foundation, Inc., 675
 * Mass Ave, Cambridge, MA 02139, USA).
*/

func curses{}
/* DOCUMENT curses package
   The curses plugin wraps some of the curses routine, hopefully enough
   of them to do something useful in a one terminal environment (i.e.
   I did not wrap multi-terminal curses control routines like newterm).

   Curses allow control of the screen and keyboard in a unbuffered way.
   you can position and write string wherever you want, without or with
   scroll, and you can monitor keyboard events. This can be useful to
   report results of large computations without the rapid screen scrolling
   and/or steer simulations as they go.

   You can see examples of use in the check-plug.i file or in the
   "progressbar" or "countdown" routines below.

   NOTE: This routine uses the "curses" library. Thus, it will work
   on Linux and maxosx machines (and probably unix) but *NOT* on windows
   machine (unless curses has been ported to windows, which I am ignorant
   of).
   Also, curses may not have all the term files. In particular, I have 
   noticed that it doesn't like to be ran in an emacs terminal/environment.
   SEE ALSO:
 */

if (strmatch(get_env("TERM"),"emacs")) \
  error,"curses will not work in an emacs environement";

plug_in, "curses";

extern kbdinit;
/* PROTOTYPE
   void kbdinit(void)
*/
extern kbd;
/* PROTOTYPE
   int kbd(int wait)
*/
extern kbdend;
/* PROTOTYPE
   void kbdend(void)
*/
extern cgetch;
/* PROTOTYPE
   int getch(void)
*/
extern cinitscr;
/* PROTOTYPE
   int initscr(void)
*/
extern cendwin;
/* PROTOTYPE
   int endwin(void)
*/
extern cisendwin;
/* PROTOTYPE
   int isendwin(void)
*/
extern ccbreak;
/* PROTOTYPE
   int cbreak(void)
*/
extern cnocbreak;
/* PROTOTYPE
   int nocbreak(void)
*/
extern chalfdelay;
/* PROTOTYPE
   int halfdelay(int tenths)
*/
extern craw;
/* PROTOTYPE
   int raw(void)
*/
extern cnoraw;
/* PROTOTYPE
   int noraw(void)
*/
extern ctimeout;
/* PROTOTYPE
   void timeout(int delay)
*/
extern cnl;
/* PROTOTYPE
   int nl(void)
*/
extern cnonl;
/* PROTOTYPE
   int nonl(void)
*/
extern cprintw;
/* PROTOTYPE
   int printw(string fmt, string str)
*/
extern cmvprintw;
/* PROTOTYPE
   int mvprintw(int y, int x, string fmt, string str)
*/
extern crefresh;
/* PROTOTYPE
   int refresh(void)
*/
extern caddch;
/* PROTOTYPE
   int addch(char ch)
*/
extern cmvaddch;
/* PROTOTYPE
   int mvaddch(int y, int x, char ch)
*/
extern cerase;
/* PROTOTYPE
   int erase(void)
*/
extern cclear;
/* PROTOTYPE
   int clear(void)
*/
extern cclrtobot;
/* PROTOTYPE
   int clrtobot(void)
*/
extern cclrtoeol;
/* PROTOTYPE
   int clrtoeol(void)
*/
extern cbkgdset;
/* PROTOTYPE
   int bkgdset(char ch)
*/
extern cgetlines;
/* PROTOTYPE
   int getlines(void)
*/
extern cgetcols;
/* PROTOTYPE
   int getcols(void)
*/
extern ccurs_set;
/* PROTOTYPE
  int curs_set(int visibility)
*/
extern ccurs_move;
/* PROTOTYPE
  int move(int y, int x)
*/
extern cbeep;
/* PROTOTYPE
   int beep(void)
*/
extern cflash;
/* PROTOTYPE
   int flash(void)
*/
extern cecho;
/* PROTOTYPE
   int echo(void)
*/
extern cnoecho;
/* PROTOTYPE
   int noecho(void)
*/
extern cgetstr;
/* PROTOTYPE
   int getstr(string str);
*/
extern cmvgetstr;
/* PROTOTYPE
   int mvgetstr(int y, int x, string str);
*/
extern cnodelay;
/* PROTOTYPE
   int ynodelay(int condition)
*/
extern cnotimeout;
/* PROTOTYPE
   int ynotimeout(int condition)
*/
extern ckeypad;
/* PROTOTYPE
   int ykeypad(int condition)
*/
extern cmeta;
/* PROTOTYPE
   int ymeta(int condition)
*/
extern cintrflush;
/* PROTOTYPE
   int yintrflush(int condition)
*/
extern cscrollok;
/* PROTOTYPE
   int yscrollok(int condition)
*/
extern cleaveok;
/* PROTOTYPE
   int yleaveok(int condition)
*/
extern cclearok;
/* PROTOTYPE
   int yclearok(int condition)
*/
extern cidlok;
/* PROTOTYPE
   int yidlok(int condition)
*/


func countdown(sec,interval)
/* DOCUMENT func countdown(sec,interval)
   sec: number of seconds to coundown
   interval: time to pause between refresh, in ms
   SEE ALSO:
 */
{
  if (!interval) interval=10;
  kbdinit;
  cerase;
  tic;
  while ((elapsed=tac()) < sec) {
    el = round(elapsed);
    cmvprintw,0,0,"[%-"+swrite(format="%s",sec)+"s]",((el==0)?"":array("*",el)(sum));
    cmvprintw,0,sec+3,"Time remaining: %s sec",swrite(format="%.3f",sec-elapsed);
    cclrtobot;
    crefresh;
    pause,interval;
  }
  kbdend;
}

func progressbar(current,outof,nstars,counter=,line=)
/* DOCUMENT print out a progress bar in the terminal using curse
   no init necessary. Just do a tic(counter) at the start of the loop.
   term has to be in curse mode.
   SEE ALSO:
 */
{
  if (!counter) counter=1;
  if (line==[]) line=0;
  nc = cgetcols();

  el = round(current*1./outof*nstars);
  cmvprintw,line,nc-nstars-2,"[%-"+swrite(format="%d",nstars)+"s]",
    ((el==0)?"":array("*",el)(sum));

  elapsed = tac(counter);
  left = elapsed*(outof-current)/(current+1e-6);
  cmvprintw,line+1,nc-nstars-2,"%s s left  ",swrite(format="%.3f",left);
  cmvprintw,line+2,nc-nstars-2,"%s",swrite(format="%d/%d",current,outof);
  cclrtoeol;
}

